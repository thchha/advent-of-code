#!/bin/perl
# Dive!
# part two.

use strict;
use warnings;

my $x = 0;
my $z = 0;
my $aim = 0;

my %cmds = (
  forward => sub {
    $x += $_[0];
    $z += $aim * $_[0];
  },
  up      => sub { $aim -= shift; },
  down    => sub { $aim += shift; },
);

while( <> ) {
  my @cmd = split('\s+', $_);
  $cmds{$cmd[0]}->($cmd[1]);
}

print $x * $z;
