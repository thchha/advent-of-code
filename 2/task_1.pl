#!/bin/perl
# Dive!

use strict;
use warnings;

my $x = 0;
my $z = 0;

my %cmds = (
  forward => sub { $x += shift; },
  up      => sub { $z -= shift; },
  down    => sub { $z += shift; },
);

while( <> ) {
  my @cmd = split('\s+', $_);
  $cmds{$cmd[0]}->($cmd[1]);
}

print $x * $z;
