#!/bin/perl
# second part

use strict;
use warnings;

my @stack;
my $count = 0;

while ( <> ) {
  $count++ if (@stack == 3 && shift @stack < $_);
  push @stack, $_;
}

print $count;
