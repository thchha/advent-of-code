#!/bin/perl
# first part

use strict;
use warnings;

# count of times the depth measurement increased
my $count = 0;

# the previous measurement.
my $prev;

while (<>) {
  $count++ if (defined $prev && $prev < $_);
  $prev = $_;
}

print $count;
