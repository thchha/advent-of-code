#!/bin/perl
# Binary Diagnostics

use strict;
use warnings;

my $ary = 12;
my $max = 2 ** $ary - 1;


my $ct = 0;

my @histogram;
my $gamma = 0;
my $epsilon = 0;

for my $i (1..$ary) { push(@histogram, 0); }

while ( <> ) {
  $ct++;
  chomp $_;
  my $binary = oct( '0b' . $_ );
  for my $i (1..$ary) {
    $histogram[$i]++ if (($binary >> ($ary - $i)) & 1);
  }
}

$ct /= 2;

for my $i (1..$ary) {
  $gamma |= ($histogram[$i - 1] >= $ct ? 1 : 0);
  $gamma <<= 1;
}

$epsilon = $max & ~$gamma;
print $gamma * $epsilon;

